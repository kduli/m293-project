
let shop = document.getElementById("shop");

let basket = JSON.parse(localStorage.getItem("data")) || [];

let generateShop = () => {
    return (shop.innerHTML = shopItemsData.map((x) => {
        let { id, name, price, ingredients, img } = x;
        let search = basket.find((x) => x.id === id) || [];
        var path = window.location.pathname;
        if (path == "/htmls/salad.html") {
            return `
                <div id=product-id-${id} class="item">
                    <img class="image" src=${img} alt="Delicious!">
                    <div class="itemInfo">
                        <h3>${name}</h3>
                        <p>${ingredients}</p>

                        <div class="dressing-price-quantity">
                            <select class="selector">
                                <option value="italian_dressing">Italian dressing</option>
                                <option value="french_dressing">French dressing</option>
                                <option value="no_dressing">No dressing</option>
                            </select>
                            <h2>$ ${price}</h2>
                            <div class="buttons">
                                <i onclick="decrement(${id})" class="bi bi-dash-lg"></i>
                                <div id=${id} class="quantity">${search.item === undefined ? 0 : search.item}</div>
                                <i onclick="increment(${id})" class="bi bi-plus-lg"></i>
                            </div>
                        </div>

                    </div>
                </div>
        `;
        }
        else if (path == "/htmls/pizza.html") {
            return `
                <div id=product-id-${id} class="item">
                    <img class="image" src=${img} alt="Delicious!">
                    <div class="itemInfo">
                        <h3>${name}</h3>
                        <p>${ingredients}</p>
                        <div class="dressing-price-quantity">
                            <h2>$ ${price}</h2>
                            <div class="buttons">
                                <i onclick="decrement(${id})" class="bi bi-dash-lg"></i>
                                <div id=${id} class="quantity">${search.item === undefined ? 0 : search.item}</div>
                                <i onclick="increment(${id})" class="bi bi-plus-lg"></i>
                            </div>
                        </div>

                    </div>
                </div>
        `;
        }
        else if (path == "/htmls/softdrink.html") {
            return `
                <div id=product-id-${id} class="item">
                    <img class="image" src=${img} alt="Delicious!">
                    <div class="itemInfo">
                        <h3>${name}</h3>
                        <p>${ingredients}</p>
                        <div class="dressing-price-quantity">
                            <h2>$ ${price}</h2>
                            <div class="buttons">
                                <i onclick="decrement(${id})" class="bi bi-dash-lg"></i>
                                <div id=${id} class="quantity">${search.item === undefined ? 0 : search.item}</div>
                                <i onclick="increment(${id})" class="bi bi-plus-lg"></i>
                            </div>
                        </div>

                    </div>
                </div>
        `;
        }

    }).join(""));
}


generateShop();


let increment = (id) => {
    let selectedItem = id;
    let search = basket.find((x) => x.id === selectedItem)

    if (search === undefined) {
        basket.push({
            id: selectedItem,
            item: 1,
        });
    }
    else {
        search.item++;
    }

    update(selectedItem);

    localStorage.setItem("data", JSON.stringify(basket));
}


let decrement = (id) => {
    let selectedItem = id;
    let search = basket.find((x) => x.id === selectedItem);

    if (search === undefined) return;
    else if (search.item === 0) return;
    else {
        search.item--;
    }

    // console.log(basket);
    update(selectedItem);
    basket = basket.filter((x) => x.item !== 0);

    localStorage.setItem("data", JSON.stringify(basket));
}


let update = (id) => {
    let search = basket.find((x) => x.id === id);
    document.getElementById(id).innerHTML = search.item;
    calculation();
}

let calculation = () => {
    let cartIcon = document.getElementById("cartAmount");
    cartIcon.innerHTML = basket.map((x) => x.item).reduce((x, y) => x + y, 0);
}

calculation(); // update the cartIcon after refreshing the page