let shopItemsData = [
    {
        id: 5,
        name: "Piccante",
        price: 16,
        ingredients: ["Tomato, Mozzarella, Spicy Salami, Chilies, Oregano"],
        img: "https://farm5.staticflickr.com/4042/4660357797_09dcd917b1.jpg"
    },
    {
        id: 6,
        name: "Giardino",
        price: 14,
        ingredients: ["Tomato, Mozzarella, Artichokes, Fresh Mushrooms"],
        img: "https://farm4.staticflickr.com/3565/5818252079_29635c03cc.jpg"
    },
    {
        id: 7,
        name: "Prosciuotto e funghi",
        price: 15,
        ingredients: ["Tomato, Mozzarella, Ham, Fresh Mushrooms, Oregano"],
        img: "https://farm9.staticflickr.com/8326/8096659940_4e0a65e598.jpg"
    },
    {
        id: 8,
        name: "Quattro formaggi",
        price: 13,
        ingredients: ["Tomato, Mozzarella, Parmesan, Gorgonzola"],
        img: "https://farm3.staticflickr.com/2797/4344770705_b6d159f799.jpg"
    },
    {
        id: 9,
        name: "Quattro stagioni",
        price: 17,
        ingredients: ["Tomato, Mozzarella, Ham, Artichokes, Fresh Mushrooms"],
        img: "https://farm5.staticflickr.com/4078/4932649252_b0aaa733ae.jpg"
    },
    {
        id: 10,
        name: "Stromboli",
        price: 12,
        ingredients: ["Tomato, Mozzarella, Fresh Chilies, Olives, Oregano"],
        img: "https://farm6.staticflickr.com/5661/22907779119_b2ec1efa11.jpg"
    },
    {
        id: 11,
        name: "Verde",
        price: 13,
        ingredients: ["Tomato, Mozzarella, Broccoli, Spinach, Oregano"],
        img: "https://farm7.staticflickr.com/6044/6363618775_e8714fb517.jpg"
    },
    {
        id: 12,
        name: "Rustica",
        price: 15,
        ingredients: ["Tomato, Mozzarella, Ham, Bacon, Onions, Garlic, Oregano"],
        img: "https://farm9.staticflickr.com/8574/16488100040_988f0caa70.jpg"
    },
];