let label = document.getElementById("label");
let ShoppingCart = document.getElementById("shopping-cart");

let basket = JSON.parse(localStorage.getItem("data")) || [];

let pizzas = JSON.parse(localStorage.getItem("data")) || [];

let calculation = () => {
    let cartIcon = document.getElementById("cartAmount");
    cartIcon.innerHTML = basket.map((x) => x.item).reduce((x,y) => x+y,0);
};

calculation();

let generateCartItems = () => {
    if(basket.length !== 0){ // local storage isn't empty
        return (ShoppingCart.innerHTML = basket.map((x) => { // x targets every object one by one
            let {id, item} = x;
            let search = shopItemsData.find((y) => y.id === id) || [];
            let {img, name, price} = search;
            return `
            <div class="cart-item">
                <img class="cartItemImage" src=${img} alt="">
                <div class="itemInfo">

                    <div class="title-price-x">
                        <h4 class="title-price">
                            <p>${name}</p>
                            <p class="cart-item-price">$ ${price}</p>
                        </h4>
                        <i onclick="removeItem(${id})" class="bi bi-x-lg"></i>
                    </div>

                    <div class="buttons">
                        <i onclick="decrement(${id})" class="bi bi-dash-lg"></i>
                        <div id=${id} class="quantity">${item}</div>
                        <i onclick="increment(${id})" class="bi bi-plus-lg"></i>
                    </div>

                    <h3>$ ${item * search.price}</h3>

                </div>
            </div>
            `;
        }).join(""));
    }
    else{ // local storage is empty
        ShoppingCart.innerHTML = ``;
        label.innerHTML = `
        <div class="empty">
            <h2>Cart is empty</h2>
            <a href="\index.html">
                <button class="homebutton">Back to home</button>
            </a>
        </div>    
        `;
    }
}

generateCartItems();


let increment = (id) => {
    let selectedItem = id;
    let search = basket.find((x)=> x.id === selectedItem)

    if(search === undefined){
        basket.push({
            id: selectedItem,
            item: 1,
        });
    }
    else{
        search.item++;
    }
    
    generateCartItems();
    update(selectedItem);
    localStorage.setItem("data", JSON.stringify(basket));
}


let decrement = (id) => {
    let selectedItem = id;
    let search = basket.find((x) => x.id === selectedItem);

    if(search === undefined) return;
    else if(search.item === 0) return;
    else{
        search.item--;
    }
    
    update(selectedItem);
    basket = basket.filter((x) => x.item !== 0);
    generateCartItems();
    localStorage.setItem("data", JSON.stringify(basket));
}

let update = (id) => {
    let search = basket.find((x) => x.id === id);
    // console.log(search.item);
    document.getElementById(id).innerHTML = search.item;
    calculation();
    totalAmount();
}

let removeItem = (id) => {
    let selectedItem = id;
    // console.log(selectedItem);
    basket = basket.filter((x) => x.id !== selectedItem);
    generateCartItems();
    totalAmount();
    calculation();
    localStorage.setItem("data", JSON.stringify(basket));
}

let clearCart = () => {
    basket = [];
    generateCartItems();
    calculation();
    localStorage.setItem("data", JSON.stringify(basket));
}

let checkoutCart = () => {
    document.getElementById("label").style.display = "none";
    document.getElementById("shopping-cart").style.display = "none";
    document.getElementById("cartAmount").innerHTML = "0";
    basket = [];
    localStorage.setItem("data", JSON.stringify(basket));
    document.getElementById("checkoutMessage").innerHTML = `<h1 class="bodyTitle">Thanks for your purchase!</h1>`;

    
}

let totalAmount = () => {
    if(basket.length !== 0){
        let amount = basket.map((x) => {
            let {item, id} = x;
            let search = shopItemsData.find((y) => y.id === id) || [];
            return item * search.price;
        }).reduce((x,y) => x+y, 0);
        // console.log(amount);
        label.innerHTML = `
        <h2 id="totalBill">Total Bill: $ ${amount}</h2>
        <button class="checkout" onclick="checkoutCart()">Checkout</button>
        <button id="removeAllBtn" onclick="clearCart()" class="removeAll">Clear cart</button>
        `;
    }
    else return;   // stop the process
}

totalAmount();

function submitForm() {
    const form = document.querySelector(".form");
    if(form.checkValidity()){
        document.querySelector(".pageTitle").innerHTML = ``;
        document.querySelector(".mainF").innerHTML = `<h1>Thanks for your feedback!</h1>`;
    }
}