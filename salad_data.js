let shopItemsData = [
    {
        id: 1,
        name: "Green salad with tomatoe",
        price: 4,
        ingredients: ["Iceberg lettuce, tomatoes"],
        img: "https://farm6.staticflickr.com/5087/5358599242_7251dc7de4.jpg",
    },
    {
        id: 2,
        name: "Tomato salad with mozzarella",
        price: 5,
        ingredients: ["Tomatoes, mozzarella"],
        img: "https://farm4.staticflickr.com/3130/5862973974_c107ed81ea.jpg"
    },
    {
        id: 3,
        name: "Field salad with egg",
        price: 4,
        ingredients: ["Field salad, eggs"],
        img: "https://farm9.staticflickr.com/8223/8372222471_662acd24f6.jpg"
    },
    {
        id: 4,
        name: "Rocket with parmesan",
        price: 5,
        ingredients: ["Rocket, parmesan"],
        img: "https://farm8.staticflickr.com/7017/6818343859_bb69394ff2.jpg"
    },
];